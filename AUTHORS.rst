Credits
=======

Development Lead
----------------

* Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>

Contributors
------------

None yet. Why not be the first?
